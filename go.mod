module gitlab.com/d-hassel-home/timetable-store

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/golang-migrate/migrate/v4 v4.3.1
	github.com/lib/pq v1.1.1
	github.com/stretchr/testify v1.3.0
)
