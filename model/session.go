package model

import (
	"time"
)

type Session struct {
	ID    int
	Name  string
	Times []*SessionTime
}

type SessionTime struct {
	From time.Time
	To   time.Time
}
