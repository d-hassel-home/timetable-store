package model

import "time"

type TimeSet struct {
	ProjectRef         string
	ProjectDescription string
	At                 time.Time
	Duration           []uint8
}
