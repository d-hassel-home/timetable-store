CREATE OR REPLACE FUNCTION create_trigger_timetable_insert()
  RETURNS trigger AS
$$
BEGIN
    IF NOT EXISTS (SELECT id from project where projectName = NEW.projectName) 
    THEN
         INSERT INTO project(id, projectName)
         VALUES(uuid_generate_v4(),NEW.projectName);
    END IF;
    INSERT INTO  timetable (session_id, project_id)
    VALUES (NEW.session_id, (SELECT id from project where projectName = NEW.projectName));
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';