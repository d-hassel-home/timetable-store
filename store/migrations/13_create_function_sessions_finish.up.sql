CREATE OR REPLACE FUNCTION public.finish_session (IN session_id integer, IN project text)
    RETURNS integer
    AS $$
DECLARE
    time_ident integer;
DECLARE
    count_id integer;
BEGIN
    SELECT count(s.time_id) INTO count_id FROM session_times s WHERE s.session_id = $1 AND date_to IS NULL;
	IF count_id > 0 THEN
		SELECT s.time_id INTO time_ident FROM session_times s WHERE s.session_id = $1 AND date_to IS NULL;
        UPDATE session_times s SET date_to = now() WHERE s.time_id = time_ident;
    END IF;
    IF NOT EXISTS (SELECT * FROM timetable_view t WHERE t.session_id = $1) THEN
        INSERT INTO timetable_view (session_id, projectName) values ($1, $2);
    END IF;
    RETURN time_ident;
END;
$$
LANGUAGE plpgsql;

