CREATE TRIGGER time_session_table_insert
  AFTER INSERT
  ON time_session
  FOR EACH ROW
  EXECUTE PROCEDURE create_trigger_sessions_insert();