CREATE TABLE IF NOT EXISTS session_times (
                time_id SERIAL,
                date_from TIMESTAMP,
                date_to TIMESTAMP,
                session_id INTEGER,
                PRIMARY KEY (time_id),
                FOREIGN KEY (session_id) REFERENCES time_session (session_id))
