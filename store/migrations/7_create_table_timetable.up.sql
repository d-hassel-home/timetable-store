CREATE TABLE IF NOT EXISTS timetable ( 
    session_id int, 
    project_id uuid ,
    FOREIGN KEY (project_id) REFERENCES project(id),
    FOREIGN KEY (session_id) REFERENCES time_session(session_id)
)