CREATE TRIGGER timetable_view_insert
  INSTEAD OF INSERT
  ON timetable_view
  FOR EACH ROW
  EXECUTE PROCEDURE create_trigger_timetable_insert();