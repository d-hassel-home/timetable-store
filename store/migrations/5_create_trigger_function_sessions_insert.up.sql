CREATE OR REPLACE FUNCTION create_trigger_sessions_insert()
  RETURNS trigger AS
$$
BEGIN
    NEW.session_id = (SELECT currval(pg_get_serial_sequence('time_session','session_id')));
    INSERT INTO  session_times (date_from, session_id)
    VALUES (now(),NEW.session_id);
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';