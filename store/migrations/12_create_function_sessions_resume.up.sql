CREATE OR REPLACE FUNCTION public.resume_session (IN session_id integer)
    RETURNS integer
    AS $$
DECLARE
    time_ident integer;
DECLARE
    count_id integer;
BEGIN
    SELECT count(s.time_id) INTO count_id FROM session_times s WHERE s.session_id = $1 AND date_to IS NULL;
	IF count_id = 0 THEN
        INSERT INTO session_times (session_id, date_from) values ($1, now());
        SELECT currval(pg_get_serial_sequence('session_times','time_id')) INTO time_ident;
    END IF;
    RETURN time_ident;
END;
$$
LANGUAGE plpgsql;

