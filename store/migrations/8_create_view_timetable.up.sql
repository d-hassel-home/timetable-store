CREATE OR REPLACE VIEW timetable_view AS
SELECT
    t.session_id AS session_id,
    p.projectName AS projectName,
    p.projectDescription AS projectDescription,
    min(
        s.date_from) AS date_at,
    sum(
        age(s.date_from, s.date_to)) AS duration
FROM
    timetable t,
    project p,
    session_times s
WHERE
    t.project_id = p.id
    AND s.session_id = t.session_id
GROUP BY t.session_id, p.projectName, p.projectDescription
