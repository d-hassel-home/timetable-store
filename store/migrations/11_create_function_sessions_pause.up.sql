CREATE OR REPLACE FUNCTION public.pause_session (IN session_id integer)
    RETURNS integer
    AS $$
DECLARE
    time_ident integer;
DECLARE
    count_id integer;
BEGIN
    SELECT count(s.time_id) INTO count_id FROM session_times s WHERE s.session_id = $1 AND date_to IS NULL;
	IF count_id = 1 THEN
        SELECT s.time_id INTO time_ident FROM session_times s WHERE s.session_id = $1 AND date_to IS NULL;
        UPDATE session_times s SET date_to = now() WHERE s.time_id = time_ident;
    ELSIF count_id > 1 THEN
        RAISE unique_violation USING MESSAGE = 'Incorrect state of session_times for session_id: ' || session_id;
    END IF;
    RETURN time_ident;
END;
$$
LANGUAGE plpgsql;

