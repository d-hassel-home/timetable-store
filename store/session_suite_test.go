package store_test

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	store "gitlab.com/d-hassel-home/timetable-store/store"
)

type sessionSuiteTest struct {
	PostgresqlSuite
}

func TestSessionSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip category mysql repository test")
	}
	dsn := os.Getenv("MYSQL_TEST_URL")
	if dsn == "" {
		dsn = fmt.Sprintf("postgres://%s:%s@%s:%d/postgres?sslmode=disable", user, password, host, port)
		/*fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbName)*/
	}

	sessionSuite := &sessionSuiteTest{
		PostgresqlSuite{
			DSN:                     dsn,
			MigrationLocationFolder: "./migrations",
		},
	}

	suite.Run(t, sessionSuite)
}

// using for random names "github.com/Pallinder/go-randomdata"

func (s *sessionSuiteTest) TestStartingSessions() {
	sessions := store.NewSessions(s.DBConn)

	s.T().Run("testing to start a new session", func(t *testing.T) {
		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 1, id, "first item should always get id 0")
	})
}

func (s *sessionSuiteTest) TestSartingSessionAndFindingIt() {
	sessions := store.NewSessions(s.DBConn)

	s.T().Run("testing to start a new session and find it", func(t *testing.T) {
		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 1, id, "first item should always get id 1")

		result, err := sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 1)
		require.Equal(t, result[0].ID, 1)
		require.NotEmpty(t, result[0].Name)
		require.NotEmpty(t, result[0].Times)
		require.Empty(t, result[0].Times[0].To)
	})
}

func (s *sessionSuiteTest) TestStartingASecondSession() {
	sessions := store.NewSessions(s.DBConn)

	s.T().Run("testing to start a second session", func(t *testing.T) {
		_, err := sessions.StartSession()
		require.NoError(t, err)
		_, err = sessions.StartSession()
		require.NoError(t, err)
	})
}

func (s *sessionSuiteTest) TestStartAndPausingSession() {
	sessions := store.NewSessions(s.DBConn)

	s.T().Run("testing to start a new session and pause it", func(t *testing.T) {
		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 1, id, "first item should always get id 1")

		result, err := sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 1)
		require.Equal(t, result[0].ID, 1)
		require.NotEmpty(t, result[0].Name)
		require.NotEmpty(t, result[0].Times)
		require.Empty(t, result[0].Times[0].To)

		err = sessions.PauseSession(id)
		require.NoError(t, err)

		result, err = sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 1)
		require.Equal(t, result[0].ID, 1)
		require.NotEmpty(t, result[0].Name)
		require.NotEmpty(t, result[0].Times)
		require.NotEmpty(t, result[0].Times[0].To)
	})

	s.T().Run("testing to start a new session and pause it twice", func(t *testing.T) {

		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 2, id, "second item should always get id 2")

		result, err := sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 2)
		require.Equal(t, result[1].ID, 2)
		require.NotEmpty(t, result[1].Name)
		require.NotEmpty(t, result[1].Times)
		require.Empty(t, result[1].Times[0].To)

		err = sessions.PauseSession(id)
		require.NoError(t, err)

		err = sessions.PauseSession(id)
		require.NoError(t, err)

	})

	s.T().Run("testing to pause a session with two open session_times should fail", func(t *testing.T) {
		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 3, id, "third item should always get id 3")

		result, err := sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 3)
		require.Equal(t, result[2].ID, 3)
		require.NotEmpty(t, result[2].Name)
		require.NotEmpty(t, result[2].Times)
		require.Empty(t, result[2].Times[0].To)

		tx, _ := s.DBConn.Begin()
		tx.Exec("INSERT INTO  session_times (date_from, session_id) VALUES (now(),$1)", id)
		tx.Commit()

		err = sessions.PauseSession(id)
		require.Error(t, err)
		require.Errorf(t, err, "Incorrect state of session_times for session_id: 3")
	})
}

func (s *sessionSuiteTest) TestResumeAPausedSession() {
	sessions := store.NewSessions(s.DBConn)

	s.T().Run("testing to pause a session and resume it", func(t *testing.T) {
		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 1, id, "first item should always get id 1")

		result, err := sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 1)
		require.Equal(t, result[0].ID, 1)
		require.NotEmpty(t, result[0].Name)
		require.NotEmpty(t, result[0].Times)
		require.Empty(t, result[0].Times[0].To)

		err = sessions.PauseSession(id)
		require.NoError(t, err)

		result, err = sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 1)
		require.Equal(t, result[0].ID, 1)
		require.NotEmpty(t, result[0].Name)
		require.NotEmpty(t, result[0].Times)
		require.NotEmpty(t, result[0].Times[0].To)

		err = sessions.ResumeSession(id)
		require.NoError(t, err)

		result, err = sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 1)
		require.Equal(t, result[0].ID, 1)
		require.NotEmpty(t, result[0].Name)
		require.NotEmpty(t, result[0].Times)
		require.NotEmpty(t, result[0].Times[0].To)
		require.Empty(t, result[0].Times[1].To)
	})

	s.T().Run("testing to pause a session and resume it twice", func(t *testing.T) {
		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 2, id, "second item should always get id 2")

		result, err := sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 2)
		require.Equal(t, result[1].ID, 2)
		require.NotEmpty(t, result[1].Name)
		require.NotEmpty(t, result[1].Times)
		require.Empty(t, result[1].Times[0].To)

		err = sessions.PauseSession(id)
		require.NoError(t, err)

		result, err = sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 2)
		require.Equal(t, result[1].ID, 2)
		require.NotEmpty(t, result[1].Name)
		require.NotEmpty(t, result[1].Times)
		require.NotEmpty(t, result[1].Times[0].To)

		err = sessions.ResumeSession(id)
		require.NoError(t, err)

		err = sessions.ResumeSession(id)
		require.NoError(t, err)

		result, err = sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, len(result), 2)
		require.Equal(t, result[1].ID, 2)
		require.NotEmpty(t, result[1].Name)
		require.Equal(t, len(result[1].Times), 2)
	})
}

func (s *sessionSuiteTest) TestFinishSession() {
	sessions := store.NewSessions(s.DBConn)
	timetable, _ := store.New(s.DBConn)

	s.T().Run("testing to finish a started session", func(t *testing.T) {
		id, err := sessions.StartSession()
		require.NoError(t, err)
		require.Equal(t, 1, id, "first item should always get id 1")

		result, err := sessions.FindSessions()
		require.NoError(t, err)
		require.Equal(t, 1, len(result))
		require.Equal(t, 1, result[0].ID)
		require.NotEmpty(t, result[0].Name)
		require.NotEmpty(t, result[0].Times)
		require.Empty(t, result[0].Times[0].To)

		err = sessions.FinishSession(id, "test")
		require.NoError(t, err)

		now := time.Now()
		results, err := timetable.Find(
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()-3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()+3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()), "test")
		require.NoError(t, err)
		require.Equal(t, 1, len(results))

	})
}
