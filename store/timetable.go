package store

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq" //using only postgresql

	model "gitlab.com/d-hassel-home/timetable-store/model"
)

// TimeTable is the crud adapter to the persistance layer.
type TimeTable struct {
	db *sql.DB
}

// New creates a new TimeTableStore.
func New(db *sql.DB) (*TimeTable, error) {
	tt := &TimeTable{db: db}
	/*err := tt.init()
	if err != nil {
		return tt, err
	}*/
	return tt, nil
}

// Find searches for mode.TimeSet in the linked postgresql database.
// The time window is defined by 'from' and 'to'.
// If ypu leave the 'projectName' empty they will not be filtered.
func (t *TimeTable) Find(from time.Time, to time.Time, projectName string) ([]*model.TimeSet, error) {
	var list []*model.TimeSet
	sqlStatementFilterd := `Select projectName, projectDescription, date_at, duration From timetable_view where projectName = $3 and date_at >= $1 and date_at <= $2`
	sqlStatement := `Select projectName, projectDescription, date_at, duration From timetable_view where date_at >= $1 and date_at <= $2`
	tx, err := t.db.Begin()
	if err != nil {
		return list, err
	}
	fmt.Println(from.String())
	fmt.Println(to.String())
	var rows *sql.Rows
	if projectName == "" {
		rows, err = tx.Query(sqlStatement, from, to)
	} else {
		rows, err = tx.Query(sqlStatementFilterd, from, to, projectName)
	}
	if err != nil {
		tx.Rollback()
		return list, err
	}
	defer rows.Close()

	list = make([]*model.TimeSet, 0)
	for rows.Next() {
		var at time.Time
		var project string
		var description sql.NullString
		var dur []uint8
		err = rows.Scan(&project, &description, &at, &dur)
		if err != nil {
			tx.Rollback()
			return list, err
		}
		list = append(list, &model.TimeSet{
			ProjectRef:         project,
			ProjectDescription: description.String,
			At:                 time.Date(at.Year(), at.Month(), at.Day(), at.Hour(), at.Minute(), at.Second(), at.Nanosecond(), time.Local),
			Duration:           dur})
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		tx.Rollback()
		return list, err
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return list, err
	}
	return list, nil
}

func (t *TimeTable) Delete(id int) error {
	sqlStatement := `DELETE FROM timetable WHERE session_id = $1`
	tx, err := t.db.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Exec(sqlStatement, id)
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}
