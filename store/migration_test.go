package store_test

import (
	"database/sql"
	"strings"

	_ "github.com/lib/pq"

	"github.com/golang-migrate/migrate/v4"
	_psql "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

type migration struct {
	Migrate *migrate.Migrate
}

func (this *migration) Up() (error, bool) {
	err := this.Migrate.Up()
	if err != nil {
		if err == migrate.ErrNoChange {
			return nil, true
		}
		return err, false
	}
	return nil, true
}

func (this *migration) Down() (error, bool) {
	err := this.Migrate.Down()
	if err != nil {
		return err, false
	}
	return nil, true
}

func runMigration(dbConn *sql.DB, migrationsFolderLocation string) (*migration, error) {
	dataPath := []string{}
	dataPath = append(dataPath, "file://")
	dataPath = append(dataPath, migrationsFolderLocation)

	pathToMigrate := strings.Join(dataPath, "")

	driver, err := _psql.WithInstance(dbConn, &_psql.Config{})
	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithDatabaseInstance(pathToMigrate, "postgres", driver)
	if err != nil {
		return nil, err
	}

	//m.Force(0)
	return &migration{Migrate: m}, nil
}
