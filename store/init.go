package store

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/golang-migrate/migrate/v4"
	_psql "github.com/golang-migrate/migrate/v4/database/postgres"
)

type migration struct {
	Migrate *migrate.Migrate
}

func InitDatabase(db *sql.DB) error {

	migrations, err := runMigration(db, "./migrations")
	if err != nil {
		return err
	}
	err, result := migrations.up()
	if err != nil {
		return err
	}
	if !result {
		return fmt.Errorf("Migrations could not successfully upgraded database schema")
	}
	return nil
}

func runMigration(dbConn *sql.DB, migrationsFolderLocation string) (*migration, error) {
	dataPath := []string{}
	dataPath = append(dataPath, "file://")
	dataPath = append(dataPath, migrationsFolderLocation)

	pathToMigrate := strings.Join(dataPath, "")

	driver, err := _psql.WithInstance(dbConn, &_psql.Config{})
	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithDatabaseInstance(pathToMigrate, "postgres", driver)
	if err != nil {
		return nil, err
	}

	return &migration{Migrate: m}, nil
}

func (this *migration) up() (error, bool) {
	err := this.Migrate.Up()
	if err != nil {
		if err == migrate.ErrNoChange {
			return nil, true
		}
		return err, false
	}
	return nil, true
}
