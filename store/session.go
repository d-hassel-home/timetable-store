package store

import (
	"database/sql"
	"database/sql/driver"
	"github.com/Pallinder/go-randomdata"
	"time"

	model "gitlab.com/d-hassel-home/timetable-store/model"
)

type Sessions struct {
	db *sql.DB
}

func NewSessions(db *sql.DB) *Sessions {
	return &Sessions{db: db}
}

func (s *Sessions) StartSession() (sessionId int, err error) {
	sqlStatement := "INSERT INTO time_session (session_name) values ($1) RETURNING session_id"
	var id int
	name := randomdata.SillyName()
	tx, err := s.db.Begin()
	if err != nil {
		tx.Rollback()
		return id, err
	}
	err = tx.QueryRow(sqlStatement, name).Scan(&id)
	if err != nil {
		tx.Rollback()
		return id, err
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return id, err
	}
	return id, nil
}

func (s *Sessions) FindSessions() ([]*model.Session, error) {
	var emptySessions []*model.Session
	var sessions []*model.Session
	sqlStatement := `Select session_id, session_name From time_session`
	tx, err := s.db.Begin()
	if err != nil {
		return emptySessions, err
	}
	var rows *sql.Rows
	rows, err = tx.Query(sqlStatement)
	if err != nil {
		tx.Rollback()
		return emptySessions, err
	}
	defer rows.Close()

	sessions = make([]*model.Session, 0)
	for rows.Next() {
		var id int
		var name string
		err = rows.Scan(&id, &name)
		if err != nil {
			// handle this error
			tx.Rollback()
			return emptySessions, err
		}
		sessions = append(sessions, &model.Session{
			ID:   id,
			Name: name,
		})
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		tx.Rollback()
		return emptySessions, err
	}
	// get the times for each session:
	for _, se := range sessions {
		times, err := s.findTimes(se.ID, tx)
		if err != nil {
			tx.Rollback()
			return emptySessions, err
		}
		se.Times = times
	}

	err = tx.Commit()
	if err != nil {
		return emptySessions, err
	}
	return sessions, nil
}

func (s *Sessions) findTimes(id int, tx *sql.Tx) ([]*model.SessionTime, error) {
	var times = make([]*model.SessionTime, 0)
	var emptyTimes []*model.SessionTime
	sqlStatement := `Select date_from, date_to From session_times where session_id = $1 order by time_id`
	var rows *sql.Rows
	rows, err := tx.Query(sqlStatement, id)
	if err != nil {
		return emptyTimes, err
	}
	defer rows.Close()

	for rows.Next() {
		var from, to NullTime
		err = rows.Scan(&from, &to)
		if err != nil {
			// handle this error
			return emptyTimes, err
		}
		times = append(times, &model.SessionTime{
			From: from.Time,
			To:   to.Time,
		})
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return emptyTimes, err
	}
	return times, nil
}

func (s *Sessions) PauseSession(id int) error {
	sqlStatement := "SELECT pause_session($1)" //"UPDATE session_times SET date_to = now() WHERE session_id = $1 and date_to is null"
	tx, err := s.db.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}
	_, err = tx.Exec(sqlStatement, id)
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

func (s *Sessions) ResumeSession(id int) error {
	sqlStatement := "SELECT resume_session($1)" //"INSERT INTO session_times (session_id, date_from) values ($1, now())"
	tx, err := s.db.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}
	_, err = tx.Exec(sqlStatement, id)
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

func (s *Sessions) FinishSession(id int, project string) error {
	sqlStatement := "SELECT finish_session($1, $2)"
	tx, err := s.db.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}
	_, err = tx.Exec(sqlStatement, id, project)
	if err != nil {
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}

// NullTime enables us to scan even null values for time.Time
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}
