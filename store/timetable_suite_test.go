package store_test

import (
	"fmt"
	"os"

	"testing"

	"time"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	//	model "gitlab.com/d-hassel-home/timetable-store/model"
	store "gitlab.com/d-hassel-home/timetable-store/store"
)

type timetableSuiteTest struct {
	PostgresqlSuite
}

func TestTimetableSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip category mysql repository test")
	}
	dsn := os.Getenv("MYSQL_TEST_URL")
	if dsn == "" {
		dsn = fmt.Sprintf("postgres://%s:%s@%s:%d/postgres?sslmode=disable", user, password, host, port)
		/*fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbName)*/
	}

	timetableSuite := &timetableSuiteTest{
		PostgresqlSuite{
			DSN:                     dsn,
			MigrationLocationFolder: "./migrations",
		},
	}

	suite.Run(t, timetableSuite)
}

func (s *timetableSuiteTest) TestFind() {
	s.insertBulkData()
	test_store, _ := store.New(s.DBConn)
	s.T().Run("should find only timesets for project `test`", func(t *testing.T) {
		now := time.Now()
		results, err := test_store.Find(
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()-3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()+3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			"test")
		require.NoError(t, err)
		require.NotZero(t, len(results))
		for _, r := range results {
			require.Equal(t, "test", r.ProjectRef)
		}
	})

	s.T().Run("should find no timesets for non existing project", func(t *testing.T) {
		now := time.Now()
		results, err := test_store.Find(
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()-3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()+3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			"not existing")
		require.NoError(t, err)
		require.Zero(t, len(results))
	})

	s.T().Run("should find timesets for all projects", func(t *testing.T) {
		now := time.Now()
		results, err := test_store.Find(
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()-3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()+3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			"")
		require.NoError(t, err)
		require.NotZero(t, len(results))

	})
}

func (s *timetableSuiteTest) TestDelete() {
	s.insertBulkData()
	test_store, _ := store.New(s.DBConn)
	s.T().Run("should delete selected timeset", func(t *testing.T) {
		now := time.Now()
		results, err := test_store.Find(
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()-3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()+3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			"test")
		require.NoError(t, err)
		origLen := len(results)
		require.NotZero(t, origLen)
		err = test_store.Delete(3)
		require.NoError(t, err)
		results, err = test_store.Find(
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()-3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			time.Date(now.Year(), now.Month(), now.Day(), now.Hour()+3, now.Minute(), now.Second(), now.Nanosecond(), now.Location()),
			"test")
		require.NoError(t, err)
		require.Equal(t, origLen-1, len(results))
	})
	s.T().Run("should not error when delete non existing timeset", func(t *testing.T) {
		err := test_store.Delete(4)
		require.NoError(t, err)
	})
}

func (s *timetableSuiteTest) insertBulkData() {
	tx, err := s.DBConn.Begin()
	require.NoError(s.T(), err)
	// insert projects
	query := "INSERT INTO project(id, projectName) VALUES(uuid_generate_v4(),$1)"
	_, err = tx.Exec(query, "test")
	require.NoError(s.T(), err)
	_, err = tx.Exec(query, "another test")
	require.NoError(s.T(), err)

	// insert sessisons
	query = "INSERT INTO time_session (session_name) values ($1)"
	_, err = tx.Exec(query, "test_session")
	require.NoError(s.T(), err)

	query = "SELECT finish_session($1, $2)"
	_, err = tx.Exec(query, "1", "test")
	require.NoError(s.T(), err)

	query = "INSERT INTO time_session (session_name) values ($1)"
	_, err = tx.Exec(query, "test_session")
	require.NoError(s.T(), err)

	query = "SELECT finish_session($1, $2)"
	_, err = tx.Exec(query, "2", "another test")
	require.NoError(s.T(), err)

	query = "INSERT INTO time_session (session_name) values ($1)"
	_, err = tx.Exec(query, "test_session")
	require.NoError(s.T(), err)

	query = "SELECT finish_session($1, $2)"
	_, err = tx.Exec(query, "3", "test")
	require.NoError(s.T(), err)

	err = tx.Commit()
	require.NoError(s.T(), err)
}
