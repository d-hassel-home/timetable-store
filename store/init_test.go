package store_test

import (
	"fmt"
	"os"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"testing"

	_ "github.com/golang-migrate/migrate/v4/source/file"

	store "gitlab.com/d-hassel-home/timetable-store/store"
)

func TestTimetableInitSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("Skip category mysql repository test")
	}
	dsn := os.Getenv("MYSQL_TEST_URL")
	if dsn == "" {
		dsn = fmt.Sprintf("postgres://%s:%s@%s:%d/postgres?sslmode=disable", user, password, host, port)
	}

	testSuite := &PostgresqlSuite{
		DSN:                     dsn,
		MigrationLocationFolder: "./migrations",
	}

	suite.Run(t, testSuite)
}

func (s *PostgresqlSuite) TestRunMigrationsScripts() {
	s.T().Run("should install all migrations without errors", func(t *testing.T) {
		err := store.InitDatabase(s.DBConn)
		require.NoError(t, err)
	})

}
