.PHONY: dependency setup format unit-test integration-test docker-up docker-down clear 

dependency:
	@go get -v ./...

setup:
	@export PSQL_TEST_URL="host=localhost port=5432 user=postgres password=changeme dbname=postgres"   

integration-test: docker-up dependency setup
	@go test -v ./...

unit-test: dependency
	@go test -v -short ./...

format: dependency
	@go fmt ./...
	@go vet ./...

docker-up:
	@docker-compose up -d

docker-down:
	@docker-compose down

clear: docker-down